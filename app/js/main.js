document.addEventListener("DOMContentLoaded", function (event) {

    function jqueryUi() {
        let citySelection = $(".select-input");
        if(citySelection) {
            citySelection.selectmenu({
                classes: {
                    "ui-selectmenu-menu": "select-menu",
                    "ui-selectmenu-button": "select-input__btn",
                    "ui-selectmenu-icon": "select-input__icon",
                    "ui-selectmenu-text": "select-input__text"
                },
                position: { my: "top top", at: "center bottom+10" }
            });
        }
        
    };

    function showMore() {
        let seeMoreWrapper = document.querySelector(".see-more-wrapper");
        if (seeMoreWrapper) {
            $(".see-more-item").slice(0, 12).show();
            $("#seeMore").click(function (e) {
                e.preventDefault();
                $(".see-more-item:hidden").slice(0, 5).fadeIn();
                if ($(".see-more-item:hidden").length == 0) {
                    $("#seeMore").fadeOut("slow");
                }
            });
        }
    };


    function toggleClass(tClass, block) {
        block.addEventListener('click', function () {
            block.classList.toggle(tClass)
        })
    };


    function searchAnim() {
        let searchBlock = document.getElementById('search');
        searchBlock.addEventListener('click', function () {
            this.classList.add('active');
        });
        document.addEventListener('click', (e) => {
            const withinBoundaries = e.composedPath().includes(searchBlock);
            if (!withinBoundaries) {
                searchBlock.classList.remove('active');
            }
        })
    };

    function masonry() {
        let elem = document.querySelector('.masonry-grid');
        let msnry = new Masonry(elem, {
            // options
            itemSelector: '.masonry-grid__item',
            columnWidth: 50
        });

        // element argument can be a selector string
        //   for an individual element
        // let msnry = new Masonry('.grid', {
        //     // options
        // });
    }








    // let searchBlock = document.getElementById('search');
    // toggleClass('active', searchBlock);
    masonry();
    showMore();
    // jqueryUi();

});